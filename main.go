package main

import (
	"gitlab.com/thejan2009/p"
	"gitlab.com/thejan2009/q"
)

func main() {
	p.New().Greet()
	q.New().Greet()
}
